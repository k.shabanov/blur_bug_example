import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(const App());
}

class App extends StatefulWidget {
  const App({
    super.key,
  });

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  bool _isBlurEnabled = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print(state.name);

    setState(() {
      _isBlurEnabled = state != AppLifecycleState.resumed;
    });
  }

  @override
  Widget build(BuildContext context) {
    print('build $_isBlurEnabled');
    return MaterialApp(
      home: Scaffold(
        body: Stack(
          children: [
            (() {
              print('child $_isBlurEnabled');
              return const SizedBox();
            })(),
            Center(child: Text(_isBlurEnabled.toString())),
            if (_isBlurEnabled) ...[
              (() {
                print('filter $_isBlurEnabled');
                return const SizedBox();
              })(),
              BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: const SizedBox(
                  height: double.infinity,
                  width: double.infinity,
                ),
              ),
            ],
          ],
        ),
      ),
    );
  }
}
